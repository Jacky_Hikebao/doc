> 在讲解Vue的响应式原理之前，建议先去看看vue涉及到的设计模式这一篇章[传送门](./frontend/vue涉及到的设计模式.md)

#### 什么是响应式

![](./img/mvvm.png)

**简易代码**

```javascript
# DefineProperty
function defineReactive(obj, key, val) {
      Object.defineProperty(obj, key, {
        get () {
            // 这里会做一次依赖收集，然后再把对应的value返回出去
            return val;
        },
        set (newValue) {
           if(newValue === val) return;
           render();// 这里是渲染模板的逻辑
        },
        enumerable: true,
        configurable: true
      })
}

# Observer
function observer (value) {
    // 这里还会进行一次递归遍历的功能
    Object.keys(value).forEach((key) => {
        defineReactive(value, key, value[key]);
    })
}

# Vue
class Vue {
    constructor(options) {
        this._data = options.data;
        observer(this._data);
    }
}


# 使用
let app = new Vue({
    data: {
        test: 'hello world!'
    }
});
app._data.test = 'ok'; // 视图更新
```

![](./img/mvvm_1.webp)

#### 响应式原理

- 模板编译
  - 分析节点（文本节点、元素节点）
  - 分析指令（v-text、v-model）
- 数据劫持
  - 将data的属性getter、setter化，包括设置值的时候也需要将其响应式
  - 将data代理到Vue实例中去
- 添加watcher监听变化
  - 在数据发生变化的地方添加watcher（v-text、v-model）