- [发布订阅模式](#发布订阅模式)

- [观察者模式](#观察者模式)
- [交集并集](#交集并集)



> 怎么实现一个发布订阅模式呢？咋们称之为收音机模式，我将信号广播出去，然后听众们根据自己的喜好订阅喜欢的FM
>
> bus.$on('click', handler)
>
> bus.$emit('click');

#### 发布订阅模式

```typescript
/**
 * 要编写一个自定义事件
 * 1. 发布者
 * 2. 订阅者
 * 3. 消息中心
 */
class EventEmitter {
    subs: { string: (() => void)[] };

    public $on(eventType: string, handler: () => void): void {
        this.subs[eventType] = this.subs[eventType] || [];
        this.subs[eventType].push(handler);
    }

    public $emit(eventType: string): void {
        if (this.subs[eventType]) {
            this.subs[eventType].forEach(handler => handler());
        }
    }
}

let bus = new EventEmitter();
bus.$on('click', handler);
bus.$emit('click');
```



#### 观察者模式

> 怎样实现一个观察者模式呢?

```typescript
/**
 * 1. 发布者
 * 2. 观察者
 */
class Dep {
    subs: any[];

    public addSubs(sub): void {
        if (sub && sub.update) {
            this.subs.push(sub);
        }
    }

    public notify(): void {
        this.subs.forEach(sub => sub.update());
    }
}

class Watcher {
    update():void {
        console.log('update');
    }
}

let dep = new Dep();
let watcher = new Watcher();
dep.addSubs(watcher);
dep.notify();
```

![](./img/watcher&sub.webp)

> 那么发布订阅与观察者有什么区别呢？
>
> - 发布订阅拥有事件中心
> - 发布订阅模式不需要知道对方是否存在，就像广播一样。



#### 交集并集

```javascript
# 交集
new Set(arr1.filter(item => arr2.includes(item)));

# 排除
new Set(arr1.filter(item => !arr2.includes(item)));

# 并集
new Set([...arr1, ...arr2]);
```

