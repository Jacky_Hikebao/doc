> vue2.6.12 + webpack5.44.0项目的搭建，学完本章节，你应该能掌握
>
> - 学完后你应该知道整个打包流程，并知道每一个打包节点都会执行什么操作，遇到打包问题能及时响应处理
>   - 读取webpack配置
>   - 解析非JS代码的资源，使其能被JS认识并处理好
>   - 根据webpack配置的plugin，接收并处理由上一步已经处理好的资源做一些资源的整合
>   - 打包完成输出到指定的output
> - 如果从架构层面一点一滴的搭建一个前端项目

- [启动一个webpack](#启动一个webpack)

- [加入vue2模板](#加入vue2模板)

- [添加html-webpack-plugin](#添加html-webpack-plugin)

- [添加css预处理器less](#添加css预处理器less)

- [配置加载图片配置](#配置加载图片配置)

- [配置HRM热更新](#配置热更新)

  - [配置mock假数据](#配置mock假数据)
  - [如何使得static文件夹搬家](#如何使得static文件夹搬家)
  - [将一些项目基本不会变动的代码打包成dll](#将一些项目基本不会变动的代码打包成dll)

  - [配置路由](#配置路由vue-router)
  - [配置axios](#配置axios)
  - [配置vuex](#配置vuex)
  - [配置eslint](#配置eslint)
  - [配置babel](#配置babel)
  - [配置提交前进行eslint检测](#配置提交前进行eslint检测)
  - [配置项目整体的代码风格](#配置项目的整体风格)

#### 启动一个webpack

**在package.json里面加入webpack以及webpack-cli**

```javascript
{
  "devDependencies": {
    "webpack": "^5.44.0",
    "webpack-cli": "^4.7.2"
  }
}
```

**创建一个项目入口index.js**

```javascript
console.log('666')
```

**创建一个webpack.config.js,用于配置项目的入口、出口**

```javascript
const path = require('path');
module.exports = {
    // 项目的打包模式'none' | 'development' | 'production'
    mode: 'production', 
    
    // 项目入口
    entry: {
    	index: './src/demo1/index.js'
    },
   
    // 项目输出路径
    output: {
        path: path.join(process.cwd(), './src/demo1/dist')
    }
};
```

**在命令框里面输入命令: npx webpack**，输出的内容有`Done in 1.64s.`产生`dist`目录

ps：完整代码在`src/demo1`目录下

#### 加入vue2模板

**在package.json里面加入依赖**

```json
# 这里需要注意的是，vue编译成html、css、js依赖vue-loader来转换，而vue-loader又依赖vue-template-compiler解析。所以他们的三者缺一不可，而且vue和vue-template-compiler的版本发布是同步的，也就是说如果你使用的vue版本是2.6.12，那么vue-template-compiler也要是2.6.12，如果你使用的vue是2.6.14的话，vue-template-compiler也应该是2.6.14。当然，如果你使用的是3.x以上的就需要安装@vue/sfc了，这个后面会提到
{
    "devDependencies": {
        "vue": "2.6.12",
        "vue-loader": "15.9.7",
        "vue-template-compiler": "2.6.12"
  }
}
```

**安装好上面的依赖后要修改webpack.config.js，这样利用起来安装的东西**

```javascript
const path = require('path');
// vue-loader有个特殊之处就是需要手动到lib下面将plugin取出来
const VueLoaderPlugin  = require('vue-loader/lib/plugin-webpack5')
module.exports = {
      entry: {
    	index: './src/demo2/index.js'
    },
    mode: 'production',
    output: {
        path: path.join(process.cwd(), 'src/demo2/dist')
    },
    module: {
        rules: [
            {
                // 告诉webpack，如果遇扩展名为.vue的则使用vue-loader来解析
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },
    plugins: [
        // 在这里将vueLoaderPlugin插件唤醒
        new VueLoaderPlugin()
    ]
};
```

**然后我们就可以创建一个vue组件来玩耍啦**

```vue
# app.vue
<template>
    <div>测试</div>
</template>

<script>
export default {
    name: 'Test'
};
</script>
```

**为了使用创建的组件当然要在入口文件index.js里面引入啦**

```javascript
import Vue from 'vue';
import App from './app.vue';

let app = new Vue({
    render: (h) => h(App) // 这里引入我们刚刚创建好的组件
});
app.$mount('#app'); // 然后挂在到id为app的HTMLDocument上
```

**在命令框里面输入命令: npx webpack**，输出的内容有`Done in 4.64s.`产生`dist`目录。

**最后为了检验我们的产物，然后需要创建一个index.html,然后将demo2/dist/index.js**引入进去，使用浏览器打开即可

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <script defer src="./src/demo2/dist/index.js"></script>
    <div id="app"></div>
</body>
</html>
```

ps：完整代码在`src/demo2`目录下



#### 添加html-webpack-plugin

demo2最后一步是手动将webpack打包好的index.js文件放在index.html里面，然后这个步骤能不能使用工具帮我们操作呢？`html-webpack-plugin`应运而生

**在package.json里面加入html-webpack-plugin插件**

```json
{
     "devDependencies": {
    	"html-webpack-plugin": "^5.3.2"
     }
}
```

**然后我们需要修改webpack.config.js，这样利用刚下载的html-webpack-plugin插件**

```javascript
const HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = {
    plugins: [
         new HtmlWebpackPlugin({
            template: path.join(process.cwd(), 'index.html')
        })
    ]
}
```

**然后在根目录下面创建一个Index.html**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
    <div id="app"></div>
</body>
</html>
```

**在命令框里面输入命令: npx webpack**，输出的内容有`Done in 7.64s.`产生`dist`目录。里面多了一个`index.html`,这样以后就不用手动创建`index.html`和引入对应的`index.js`文件啦，因为`html-webpack-plugin`已经帮我们做好啦

ps：完整代码在`src/demo3`目录下



#### 添加css预处理器less

**在package.json里面加入依赖less less-loader css-loade style-loader**

```json
{
      "devDependencies": {
        "css-loader": "^5.2.6",
        "less": "^4.1.1",
        "less-loader": "^10.0.1",
        "style-loader": "^3.0.0"
      }
}
```

**安装好上面的依赖后要修改webpack.config.js，这样利用起来安装的东西**

```javascript
module.exports= {
     module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'less-loader'
                ]
            }
        ]
    }
}
```

**在app.vue里面使用less**

```vue
<template>
	<div><span>测试</span></div>
</template>

<script>
export default {
    name: 'Test'
};
</script>

<style lang="less">
div {
    span {
        color: green;
    }
}
</style>
```

**在命令框里面输入命令: npx webpack**，输出的内容有`Done in 7.64s.`产生`dist`目录

ps：完整代码在`src/demo4`目录下

#### 配置加载图片配置

**在package.json里面加入url-loader和file-loader**

```json
# 其中url-loader用于解析路径，file-loader用于将图片转换成base64或者其它形式的资源
{
  "devDependencies": {
    "file-loader": "^6.2.0",
    "url-loader": "^4.1.1",
}
```

**安装好上面的依赖后要修改webpack.config.js，这样利用起来安装的东西**

```javascript
module.exports = {
  module: {
      rules: [
            {
            	test: /\.(png|jpg|gif|svg|webp)$/i,
                use: [{
                        loader: 'url-loader',
                        options: {
                            limit: 8192, // 小于8192bit的图片使用base64
                            // 记得加上这个，不然会出现[object Object]
                            esModule: false 
                        },
                    },
                ],
            }]
  }
}
```

**配置好了之后就可以在app.vue里面写一些本地图片啦**

```vue
<template>
    <div>
        <span>测试</span>
        <img src="https://v3.cn.vuejs.org/images/options-api.png" alt="">
        <img src="./options-api.png" alt="">
    </div>
</template>

<script>
/**
 * Author: Hikebao
 * Date: 2021/7/10
 * Descriptions:
 */

export default {
    name: 'Test'
};
</script>

<style lang="less">
* {
    color: red;
}
div {
    background-image: url("./options-api.png");
    span {
        color: green;
    }
}
</style>
```

**在命令框里面输入命令: npx webpack**，输出的内容有`Done in 6.64s.`产生`dist`目录

ps：完整代码在`src/demo5`目录下



#### 配置热更新

**在package.json里面加入webpack-dev-server依赖，并且在script下面加多一个dev的配置**

```json
{
  "scripts": {
    "dev": "webpack serve --config ./src/demo6/webpack.config.js"
  },
  "devDependencies": {
    "webpack-dev-server": "^3.11.2"
  }
}
```

**安装好上面的依赖后要修改webpack.config.js，这样利用起来安装的东西**

```javascript
module.exports={
       devServer: {
            port: 8080,
            open: true, // 是否自动打开浏览器
            host: 'localhost', 
            hot: true, // 开启热更新
            contentBase: path.join(process.cwd(), `src/demo6/dist`),
            writeToDisk: true, // 在开发模式下可以将内存中的文件输出到磁盘
    }
}
```

**在命令框里面输入命令: yarn dev**，启动的时候会自动打开窗口

ps：完整代码在`src/demo6`目录下





> 写在最后，本章节初衷是为了令读者能够大盖了解在企业项目实战中的webpack打包流程，并没有深入去剖析每一个小结的细节以及优化技巧，那么这里要告诉各位一个好消息就是下一个章节将会剖析解答某些内容。
>
> - 引入ts
> - 资源分类，如何根据实际情况给每个资源分包
> - 前端权限控制怎么做
> - 如何优化axios请求
> - 如何优化vuex
> - 如何配置多入口打包
> - 下一代vue3